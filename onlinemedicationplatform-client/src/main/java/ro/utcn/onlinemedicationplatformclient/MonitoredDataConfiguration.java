package ro.utcn.onlinemedicationplatformclient;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class MonitoredDataConfiguration {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("ro.utcn.consumingwebservice.wsdl");
        return marshaller;
    }

    @Bean
    public MonitoredDataClient monitoredDataClient(Jaxb2Marshaller marshaller) {
        MonitoredDataClient client = new MonitoredDataClient();
        client.setDefaultUri("http://localhost:8080/ws");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

}
