package ro.utcn.onlinemedicationplatformclient;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class OnlinemedicationplatformClientApplication extends Application {

	private ConfigurableApplicationContext springContext;
	private Parent rootNode;
	private FXMLLoader fxmlLoader;
	public static Stage stage;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception{
		springContext = SpringApplication.run(OnlinemedicationplatformClientApplication.class);
		fxmlLoader = new FXMLLoader();
		fxmlLoader.setControllerFactory(springContext::getBean);
		stage = primaryStage;
		fxmlLoader.setLocation(getClass().getResource("/fxml/MainScene.fxml"));
		rootNode = fxmlLoader.load();

		primaryStage.setTitle("Medication Tracker");
		Scene scene = new Scene(rootNode, 800, 500);
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	@Override
	public void stop() {
		SpringApplication.exit(springContext);
	}

}
