package ro.utcn.onlinemedicationplatformclient;

import ro.utcn.consumingwebservice.wsdl.GetMonitoredDataRequest;
import ro.utcn.consumingwebservice.wsdl.GetMonitoredDataResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class MonitoredDataClient extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(MonitoredDataClient.class);

    public GetMonitoredDataResponse getMonitoredData(Long id) {

        GetMonitoredDataRequest request = new GetMonitoredDataRequest();
        request.setId(1);

        log.info("Requesting monitored data for patient with Id = " + id);

        GetMonitoredDataResponse response = (GetMonitoredDataResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/ws/countries", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/GetMonitoredDataRequest"));
        return response;
    }
}
