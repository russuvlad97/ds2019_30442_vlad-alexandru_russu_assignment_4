package ro.utcn.onlinemedicationplatformclient.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ChoiceBox;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ro.utcn.consumingwebservice.wsdl.GetMonitoredDataResponse;
import ro.utcn.consumingwebservice.wsdl.MonitoredDataType;
import ro.utcn.onlinemedicationplatformclient.MonitoredDataClient;
import ro.utcn.onlinemedicationplatformclient.MonitoredDataConfiguration;

import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class MainController implements Initializable {

    @FXML
    private ChoiceBox<Integer> daysChoice;

    @FXML
    private PieChart chart;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MonitoredDataConfiguration.class);
        MonitoredDataClient quoteClient = context.getBean(MonitoredDataClient.class);
        GetMonitoredDataResponse response = quoteClient.getMonitoredData(Long.valueOf(1));
        System.out.println("Distinct days: " + getDistinctDays(response.getMonitoredData()));

        Map<Integer, Map<String, Integer>> activityOccurrencesPerDay;
        activityOccurrencesPerDay = getActivityOccurrencesPerDay(response.getMonitoredData());
        activityOccurrencesPerDay.forEach((day, map)->{
            System.out.println("Day " + day + ":");
            map.forEach((key, value)->{
                System.out.println("	Activity: " + key + " lasted " + value + " minutes.");
            });
        });

        this.daysChoice.setItems(FXCollections.observableList(getDistinctDays(response.getMonitoredData())));

        daysChoice.setOnAction(e->{
            List<PieChart.Data> pieChart = new ArrayList<>();

            int day = daysChoice.getValue();
            Map<String, Integer> chartData = activityOccurrencesPerDay.get(day);
            chartData.forEach((key, value) -> {
                PieChart.Data entry = new PieChart.Data(key, value);
                pieChart.add(entry);
            });

            ObservableList<PieChart.Data> obsPieChart = FXCollections.observableList(pieChart);
            chart.setData(obsPieChart);
        });

    }

    public static <T> Predicate<T> distinctByDay(Function<? super T, Object> dayExtractor)
    {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(dayExtractor.apply(t), Boolean.TRUE) == null;
    }

    public List<Integer> getDistinctDays(List<MonitoredDataType> list){
        List<MonitoredDataType> distinctElements = list.stream()
                .filter(distinctByDay(p -> p.getStartTime().getDay()))
                .collect(Collectors.toList());

        List<Integer> distinctDays = new ArrayList<>();

        for(MonitoredDataType element : distinctElements){
            distinctDays.add(element.getStartTime().getDay());
        }

        return distinctDays;
    }

    public Map<Integer, Map<String, Integer>> getActivityOccurrencesPerDay(List<MonitoredDataType> list){
        Map<Integer, Map<String, Integer>> occurrences = new HashMap<>();

        list.forEach(e -> occurrences.put(e.getStartTime().getDay(), new HashMap<String, Integer>()));
        list.forEach(e -> {
            occurrences.get(e.getStartTime().getDay()).computeIfPresent(e.getMonitoredData(), (k, v) -> v = v + e.getDuration());
            occurrences.get(e.getStartTime().getDay()).putIfAbsent(e.getMonitoredData(), e.getDuration());
        });

        return occurrences;
    }

}
