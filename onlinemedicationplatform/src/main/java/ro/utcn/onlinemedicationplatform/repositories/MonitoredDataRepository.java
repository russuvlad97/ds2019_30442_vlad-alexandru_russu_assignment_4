package ro.utcn.onlinemedicationplatform.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.utcn.onlinemedicationplatform.entities.MonitoredData;

@Repository
public interface MonitoredDataRepository extends JpaRepository<MonitoredData, Long> {
}
