package ro.utcn.onlinemedicationplatform.entities.util;

public enum Type {
    PATIENT,
    CAREGIVER,
    DOCTOR
}
