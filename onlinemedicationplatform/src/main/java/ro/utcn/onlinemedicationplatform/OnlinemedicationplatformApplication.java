package ro.utcn.onlinemedicationplatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlinemedicationplatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlinemedicationplatformApplication.class, args);
	}

}
