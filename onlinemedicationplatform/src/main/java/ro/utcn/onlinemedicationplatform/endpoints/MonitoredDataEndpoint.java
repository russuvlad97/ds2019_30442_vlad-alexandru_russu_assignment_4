package ro.utcn.onlinemedicationplatform.endpoints;

import com.baeldung.springsoap.gen.GetMonitoredDataRequest;
import com.baeldung.springsoap.gen.GetMonitoredDataResponse;
import com.baeldung.springsoap.gen.MonitoredDataType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ro.utcn.onlinemedicationplatform.entities.MonitoredData;
import ro.utcn.onlinemedicationplatform.repositories.MonitoredDataRepository;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.time.Duration;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Endpoint
public class MonitoredDataEndpoint {

    private static final String NAMESPACE_URI = "http://www.baeldung.com/springsoap/gen";

    private MonitoredDataRepository monitoredDataRepository;

    @Autowired
    public MonitoredDataEndpoint(MonitoredDataRepository monitoredDataRepository) {
        this.monitoredDataRepository = monitoredDataRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getMonitoredDataRequest")
    @ResponsePayload
    public GetMonitoredDataResponse getActivities(@RequestPayload GetMonitoredDataRequest request) {
        GetMonitoredDataResponse response = new GetMonitoredDataResponse();

        List<MonitoredData> monitoredDataList = monitoredDataRepository.findAll();
        List<MonitoredDataType> activityList = new ArrayList<>();

        for(MonitoredData monitoredData : monitoredDataList){

            MonitoredDataType activity = new MonitoredDataType();
            activity.setMonitoredData(monitoredData.getActivity());

            XMLGregorianCalendar start;
            XMLGregorianCalendar end;

            try{
                start = DatatypeFactory.newInstance().newXMLGregorianCalendar(monitoredData.getStartTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
                end = DatatypeFactory.newInstance().newXMLGregorianCalendar(monitoredData.getEndTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
                activity.setStartTime(start);
                activity.setEndTime(end);
                activity.setDuration((int)(Duration.between(monitoredData.getStartTime(), monitoredData.getEndTime()).toMinutes()));
            }
            catch (Exception e){
                e.printStackTrace();
            }

            activityList.add(activity);

        }

        response.setMonitoredData(activityList);

        return response;
    }
}